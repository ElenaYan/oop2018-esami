package a02a.e1;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import a02a.e1.Tournament.Result;
import a02a.e1.Tournament.Type;

public class TournamentBuilderImpl implements TournamentBuilder {
	
	private Optional<Type> type = Optional.empty();
	private Optional<String> name = Optional.empty();
	private Optional<Map<String, Integer>> InitialRanking = Optional.empty();
	private Optional<Map<String, Integer>> FinalRanking = Optional.empty();
	private final Map<String, Result> results = new HashMap<>();
	private boolean isBuild = false;
	private boolean addWinner = false;
	
	private Integer maxPoints(Optional<Type> t) {
		if(t.isEmpty()) {
			throw new IllegalStateException();
		}
		else if (t.get() == Type.MAJOR) {
			return 2500;
		}
		else if (t.get() == Type.ATP1000) {
			return 1000;
		}
		else if (t.get() == Type.ATP500) {
			return 500;
		}
		else {
			return 250;
		}
	}
	
	private Integer addPoints(Result result) {
		if (result == Result.WINNER) {
			return maxPoints(this.type);
		}
		else if (result == Result.FINALIST) {
			return maxPoints(this.type)/2;
		}
		else if (result == Result.SEMIFINALIST) {
			return maxPoints(this.type)/5;
		}
		else if (result == Result.QUARTERFINALIST) {
			return maxPoints(this.type)/10;
		}
		else {
			return 0;
		}
	}

	@Override
	public TournamentBuilder setType(Type type) {
		this.type = Optional.of(type);
		return this;
	}

	@Override
	public TournamentBuilder setName(String name) {
		this.isBuild = false;
		this.addWinner = false;
		this.name = Optional.of(name);
		return this;
	}

	@Override
	public TournamentBuilder setPriorRanking(Map<String, Integer> ranking) {
		results.clear();
		this.InitialRanking = Optional.of(new HashMap<>(ranking));
		this.FinalRanking = Optional.of(new HashMap<>(ranking));
		return this;
	}

	@Override
	public TournamentBuilder addResult(String player, Result result) {
		if(result == Result.WINNER) {
			if(this.addWinner  == true) {
				throw new IllegalStateException();
			}
			else {
				this.addWinner = true;
			}
		}
		
		if(results.containsKey(player)) {
			throw new IllegalStateException();
		}
		else {
			results.put(player, result);
		}
		
		if (this.InitialRanking.isEmpty() || this.FinalRanking.isEmpty()) {
			throw new IllegalStateException();
		}
		
		if(!FinalRanking.get().containsKey(player)) {
			FinalRanking.get().put(player, 0);
		}
		
		Optional<Integer> oldPoint = Optional.empty();
		
		for(Entry<String,Integer> entry : InitialRanking.get().entrySet()) {
			if(entry.getKey().equals(player)) {
				oldPoint = Optional.ofNullable(entry.getValue());
			}
		}
		
		if(oldPoint.isPresent()) {
			FinalRanking.get().replace(player, oldPoint.get()+addPoints(result));
		}
		else {
			FinalRanking.get().replace(player, addPoints(result));
		}
		
		return this;
		
	}

	@Override
	public Tournament build() throws IllegalStateException{
		if(isBuild == false) {
			isBuild  = true;
			if(this.type.isEmpty() || this.results.isEmpty() || this.name.isEmpty() || this.InitialRanking.isEmpty() || this.FinalRanking.isEmpty()) {
				throw new IllegalStateException();
			}
			return new Tournament() {
		
				@Override
				public Type getType() {
					return type.get();
				}
		
				@Override
				public String getName() {
					return name.get();
				}
		
				@Override
				public Optional<Result> getResult(String player) {
					return Optional.ofNullable((results.get(player)));
				}
		
				@Override
				public String winner() {
					for(Entry<String, Result> entry : results.entrySet()) {
						if(entry.getValue()==Result.WINNER) {
							return entry.getKey();
						}
					}
					throw new IllegalStateException();
				}
		
				@Override
				public Map<String, Integer> initialRanking() {
					return InitialRanking.get();
				}
		
				@Override
				public Map<String, Integer> resultingRanking() {
					
					return FinalRanking.get();
				}
		
				@Override
				public List<String> rank() {
					return FinalRanking.get().entrySet().stream()
							                            .sorted((e1,e2) -> e2.getValue().compareTo(e1.getValue()))
							                            .map(e-> e.getKey())
							                            .collect(Collectors.toList());
					     
				}
				
			};
		} else {
			
			throw new IllegalStateException();
		
		}
	}
}
