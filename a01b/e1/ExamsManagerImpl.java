package a01b.e1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import java.util.stream.Collectors;


public class ExamsManagerImpl implements ExamsManager {
	
	Set<String> calls;
	Map<Pair<String,ExamResult>, String> exams;  

	public ExamsManagerImpl() {
		this.exams = new HashMap<>();
		this.calls = new HashSet<>();
	}
	
	@Override
	public void createNewCall(String call) {
		if (!this.calls.contains(call)) {
			this.calls.add(call);
		}
		else throw new IllegalArgumentException();
	}

	@Override
	public void addStudentResult(String call, String student, ExamResult result) {
		if (this.calls.contains(call) && !(this.exams.containsKey(new Pair<>(student, result)) && this.exams.containsValue(call))) {
			this.exams.put(new Pair<>(student, result), call);
		}
		else throw new IllegalArgumentException();
	}

	@Override
	public Set<String> getAllStudentsFromCall(String call) {
		 Set<String> studentsInCall = new HashSet<>();
		 this.exams.entrySet().stream()
		 					  .filter(e -> e.getValue().equals(call))
		                      .forEach(e -> studentsInCall.add(e.getKey().getFst()));
		 return studentsInCall;
	}


	@Override
	public Map<String, Integer> getEvaluationsMapFromCall(String call) {
		Map<String,Integer> evaluations = new HashMap<>();
		this.exams.entrySet().stream()
							 .filter(e -> e.getValue().equals(call))
		                     .filter(e -> e.getKey().getSnd().getEvaluation().isPresent())
		                     .forEach(e -> {
		                    	 evaluations.put(e.getKey().getFst(), e.getKey().getSnd().getEvaluation().get());
		                     });
		return evaluations;
	}

	@Override
	public Map<String, String> getResultsMapFromStudent(String student) {
		Map <String, String> studentResults = new HashMap<>();
		studentResults = this.exams.entrySet().stream()
		 					 .filter(e -> e.getKey().getFst().equals(student))
		 					 .collect(Collectors.toMap(Entry :: getValue, e -> e.getKey().getSnd().toString()));
		 					 
		System.out.println(studentResults);
		return studentResults;
	}

	@Override
	public Optional<Integer> getBestResultFromStudent(String student) {
		Optional<Integer> best = Optional.empty();
//				Optional.of(exams.entrySet()
//									  .stream()
//									  .filter(e -> (e.getKey().getFst().equals(student))
//									  .map(e -> e.getKey().getSnd())
//									 .filter(e -> e.getKey().getFst().equals(student))
//									  .mapToInt(e -> e.getKey().getSnd().getEvaluation().get())
//									  .max().getAsInt());
		System.out.println(best);
		return best;
	}

}
