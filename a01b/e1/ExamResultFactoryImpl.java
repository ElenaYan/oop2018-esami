package a01b.e1;

import java.util.Optional;

public class ExamResultFactoryImpl implements ExamResultFactory {

	@Override
	public ExamResult failed() {
		ExamResult res = new ExamResultImpl(Optional.empty(),ExamResult.Kind.FAILED);
		return res;
	}

	@Override
	public ExamResult retired() {
		return new ExamResultImpl(Optional.empty(),ExamResult.Kind.RETIRED);
	}

	@Override
	public ExamResult succeededCumLaude() {
		return new ExamResultImpl(Optional.of(30), ExamResult.Kind.SUCCEEDED);
	}

	@Override
	public ExamResult succeeded(int evaluation) {
		if(evaluation > 30 || evaluation < 18) { 
			throw new IllegalArgumentException();
		}
		return new ExamResultImpl(Optional.of(evaluation), ExamResult.Kind.SUCCEEDED);
	}

}
