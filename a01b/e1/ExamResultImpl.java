package a01b.e1;

import java.util.Optional;

public class ExamResultImpl implements ExamResult {
	private Optional<Integer> mark;
	private Kind kind;

	@Override
	public Kind getKind() {
		return this.kind;
	}

	public ExamResultImpl(Optional<Integer> mark, Kind kind) {
		if(mark.isPresent()) {
			this.mark = mark;
		} else {
			this.mark = Optional.empty();
		}
		this.kind = kind;
	}

	@Override
	public Optional<Integer> getEvaluation() {
		return this.mark;
	}


	@Override
	public boolean cumLaude() {
		if((this.kind==ExamResult.Kind.SUCCEEDED) && this.mark.get() == 30) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		String string = this.kind.toString();
		if(this.mark.isPresent()) {
			if(cumLaude()) {
				string = string + "(" + this.mark.get() + "L" +  ")";
			}
			else {
				string = string + "(" + this.mark.get() + ")";
			}
		}
		return string;
	}


	
	

}
