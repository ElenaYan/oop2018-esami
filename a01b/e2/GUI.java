package a01b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class GUI extends JFrame {

	 /**
	 * 
	 */
	private static final long serialVersionUID = -4777437676214072980L;
	private final List<JButton> buttons = new ArrayList<>();
	    
	    public GUI() {
	        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	        this.setSize(300,300);
	        
	        JPanel panel = new JPanel(new GridLayout(3,3));
	        this.getContentPane().add(BorderLayout.CENTER,panel);
	        
	        ActionListener al = (e)->{
	            final JButton bt = (JButton)e.getSource();
	            bt.setText("pos"+buttons.indexOf(bt));
	            bt.setEnabled(false);
	        };
	                
	        for (int i=0; i<9; i++){
	            final JButton jb = new JButton(" ");
	            jb.addActionListener(al);
	            this.buttons.add(jb);
	            panel.add(jb);
	        }
	        this.setVisible(true);
    }
    
    
}
