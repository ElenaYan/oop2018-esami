package a01a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PrimitiveIterator.OfInt;
import java.util.Random;
import java.util.stream.IntStream;

import javax.swing.*;

public class GUI extends JFrame {
    
	 /**
	 * 
	 */
	private static final long serialVersionUID = 5745922659877560301L;
	
	private final Map<JButton,Pair<Integer,Integer>> buttons = new HashMap<>();
	private final Logics logics = new LogicsImpl();
	private Pair<Integer,Integer> posCavallo = null;
	private Pair<Integer,Integer> posPedone = null;
	
	private JButton cavallo = null;
	private JButton pedone = null;
	
	private Random rand = new Random();

	    
	    public GUI() {
	        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	        this.setSize(500,500);
	        
	        JPanel panel = new JPanel(new GridLayout(5,5));
	        this.getContentPane().add(BorderLayout.CENTER,panel);
	        
	        ActionListener al = (e)->{
	            JButton bt = (JButton)e.getSource();
	            Pair<Integer, Integer> pos = null;
	            
	            for(Entry<JButton,Pair<Integer,Integer>> entry : buttons.entrySet()) {
	            	if (entry.getKey().equals(bt)) {
	            		pos = entry.getValue();
	            	}
	            }
	            
	            if(logics.move(posCavallo, pos)) {
	            	cavallo.setText(" ");
	            	bt.setText("K");
	            	
	            	JButton scambio = cavallo;
	            	cavallo = bt;
	            	bt = scambio;

	            	for(Entry<JButton,Pair<Integer,Integer>> entry : buttons.entrySet()) {
	            		if(entry.getKey().equals(cavallo)) {
	            			posCavallo = entry.getValue();
	            		}
	            	}
	            }
                 
	            if(logics.isOver(posCavallo, posPedone)) {
	            	System.exit(0);
	            }
	        };
	        
	        Integer randX1 = rand.ints(0, 5).iterator().next();
	        Integer randY1 = rand.ints(0, 5).iterator().next();
	        Integer randX2 = rand.ints(0, 5).iterator().next();
	        Integer randY2 = rand.ints(0, 5).iterator().next();
	        
	        for (int i=0; i<5; i++){
		        for (int j=0; j<5; j++) {
		        	if(i==randX1 && j==randY1) {
		         		cavallo = new JButton("K");
		        		cavallo.addActionListener(al);
		        		this.buttons.put(cavallo, new Pair<>(j,i));
		        		panel.add(cavallo);
		        		this.posCavallo = new Pair<> (j,i);
		        	}
		        	else if(i==randX2 && j==randY2) {
		        		pedone = new JButton("*");
		        		pedone.addActionListener(al);
		        		this.buttons.put(pedone, new Pair<>(j,i));
		        		panel.add(pedone);
		        		this.posPedone = new Pair<> (j,i);
		        	}
		        	else {
		        		JButton jb = new JButton(" ");
		        		jb.addActionListener(al);
		        		this.buttons.put(jb, new Pair<>(j,i));
		        		panel.add(jb);
	            	}
		        }
	        }
 
	        this.setVisible(true);
    
	    }
}
