package a01a.e2;

public interface Logics {
	
	boolean move(Pair<Integer, Integer> cavallo, Pair<Integer, Integer> pos);
	
	boolean isOver(Pair<Integer, Integer> cavallo, Pair<Integer, Integer> pedone);

}
