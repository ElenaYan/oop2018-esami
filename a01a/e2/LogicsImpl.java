package a01a.e2;

public class LogicsImpl implements Logics {

	@Override
	public boolean move(Pair<Integer, Integer> cavallo, Pair<Integer, Integer> pos) {
		
		return (cavallo.getY().equals(pos.getY() -2) || cavallo.getY().equals(pos.getY() +2)) && (cavallo.getX().equals(pos.getX() -1) || cavallo.getX().equals(pos.getX() + 1))
			|| (cavallo.getY().equals(pos.getY() -1) || cavallo.getY().equals(pos.getY() +1)) && (cavallo.getX().equals(pos.getX() -2) || cavallo.getX().equals(pos.getX() + 2));
	}

	@Override
	public boolean isOver(Pair<Integer, Integer> cavallo, Pair<Integer, Integer> pedone) {
		return cavallo.getX().equals(pedone.getX()) && cavallo.getY().equals(pedone.getY());
	}

	

}
