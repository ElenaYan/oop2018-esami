package a06.e1;

import java.util.Set;

public class ParserFactoryImpl implements ParserFactory {
	
	@Override
	public Parser one(String token) {
		return new Parser() {
			
			private String acceptString = token;
			private int counter = 0;
			
			@Override
			public boolean acceptToken(String token) {
				return (this.counter++ < 1 && token.equals(this.acceptString)) ? true : false;
			}

			@Override
			public boolean inputCompleted() {
				return (this.counter==1) ? true : false;
			}

			@Override
			public void reset() {
				this.counter = 0;
			}
			
		};
	}

	@Override
	public Parser many(String token, int elemCount) {
		return new Parser() {
			 
			private String acceptString = token;
			private int counter = 0;
			
			@Override
			public boolean acceptToken(String token) {
				return (this.counter++ < elemCount && token.equals(this.acceptString)) ? true : false;
			}

			@Override
			public boolean inputCompleted() {
				return (this.counter==elemCount) ? true : false;
			}

			@Override
			public void reset() {
				this.counter = 0;
			}
			
		};
	}

	@Override
	public Parser oneOf(Set<String> set) {
		return new Parser() {

			private int counter = 0;
			
			@Override
			public boolean acceptToken(String token) {
				return (this.counter++ < 1 && set.contains(token)) ? true : false;
			}

			@Override
			public boolean inputCompleted() {
				return (this.counter==1) ? true : false;
			}

			@Override
			public void reset() {
				this.counter = 0;
			}
			
		};
	}

	@Override
	public Parser sequence(String token1, String token2) {
		return new Parser() {
			
			private int counter = 0;
			
			@Override
			public boolean acceptToken(String token) {
				return (this.counter++ < 2 && (token.equals(token1) || token.equals(token2) )) ? true : false;
			}

			@Override
			public boolean inputCompleted() {
				return (this.counter==2) ? true : false;
			}

			@Override
			public void reset() {
				this.counter = 0;
			}
			
		};
	}

	@Override
	public Parser fullSequence(String begin, Set<String> elem, String separator, String end, int elemCount) {
		return new Parser() {

			private int counter = 0;
			
			@Override
			public boolean acceptToken(String token) {
				if(this.counter==0 && token.equals(begin)) {
					this.counter++;
					return true;
				}
				else if(this.counter==2*elemCount && token.equals(end)) {
					this.counter++;
					return true;
				}
				else if (this.counter%2==1 && elem.contains(token)){
					this.counter++;
					return true;
				}
				else if (this.counter%2==0 && separator.equals(token)){
					this.counter++;
					return true;
				}
				else {
					return false;
				}
			}

			@Override
			public boolean inputCompleted() {
				return (this.counter==2*elemCount+1) ? true : false;
			}

			@Override
			public void reset() {
				this.counter=0;
			}
			
		};
	}

}
