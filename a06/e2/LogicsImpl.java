package a06.e2;

import java.util.LinkedList;
import java.util.List;

public class LogicsImpl implements Logics {
	
	private List<Integer> list;

	public LogicsImpl() {
		this.list = new LinkedList<>();
	}

	@Override
	public void init(int size) {
		for(int i =0; i <size; i++) {
			list.add(1);
		}
	}

	@Override
	public String setNumber(int position) {
		Integer sum = 0;
		for(int i=0; i<=position; i++) {
			sum = sum + list.get(i);
		}
		list.set(position, sum);
		return sum.toString();
	}

	@Override
	public String reset(int size) {
		list.clear();
		init(size);
		return "1";
	}


}
