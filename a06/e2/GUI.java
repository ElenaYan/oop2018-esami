package a06.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.stream.*;
import javax.swing.*;

import java.util.LinkedList;
import java.util.List;


public class GUI extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -440257988028088338L;
	private Logics logics = new LogicsImpl(); 
	private List<JButton> list = new LinkedList<>();
	
	public GUI(int size){
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		logics.init(size);
		
		ActionListener ac = e -> {
			final JButton jb = (JButton)e.getSource();
			jb.setEnabled(false);
			jb.setText(logics.setNumber(list.indexOf(jb)));
		};
		
		ActionListener res = e -> {
			list.forEach(l -> { 
				final JButton jb = l;
				jb.setEnabled(true);
				jb.setText(logics.reset(size));
			});
		};
			
		for(int i=0; i<size; i++) {
			JButton jb = new JButton("1");
			jb.addActionListener(ac);
			this.getContentPane().add(jb);
			list.add(jb);
		}
		
		JButton reset = new JButton("Reset");
		reset.addActionListener(res);
		this.getContentPane().add(reset);
		
		this.setVisible(true);
	}
	
}
