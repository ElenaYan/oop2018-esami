package a06.e2;

public interface Logics {
	
	void init(int size);
	
	String setNumber(int position);
	
	String reset(int size);
	
}
