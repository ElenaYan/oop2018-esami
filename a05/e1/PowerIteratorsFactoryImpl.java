package a05.e1;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.UnaryOperator;

public class PowerIteratorsFactoryImpl implements PowerIteratorsFactory {

	@Override
	public PowerIterator<Integer> incremental(int start, UnaryOperator<Integer> successive) {
		return new PowerIterator<Integer> () {

			private Integer counter = 0; 
			private List<Integer> list = new LinkedList<>();
			
			@Override
			public Optional<Integer> next() {
				Optional<Integer> ris = Optional.of(start);
				if(this.counter==0) {
					this.counter++;
				}
				else {
					for(int i=0; i<this.counter; i++) {
						ris = Optional.of(successive.apply(ris.get())); 
					}
					this.counter++;
				}
				list.add(ris.get());
				return ris;
			}

			@Override
			public List<Integer> allSoFar() {
				return this.list;
			}

			@Override
			public PowerIterator<Integer> reversed() {
				return null;
			}
			
		};
	}

	@Override
	public <X> PowerIterator<X> fromList(List<X> list) {
		return new PowerIterator<X>() {
			
			private int counter=0;
			private List<X> llist = new LinkedList<>();
			
			@Override
			public Optional<X> next() {
				if(this.counter < list.size()) {
					Optional<X> next = Optional.of(list.get(this.counter));
					this.counter++;
					this.llist.add(next.get());
					return next;
				}
				else {
					return Optional.empty();
				}
			}

			@Override
			public List<X> allSoFar() {
				return this.llist;
			}

			@Override
			public PowerIterator<X> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public PowerIterator<Boolean> randomBooleans(int size) {
		return new PowerIterator<Boolean>() {
			private int counter = 0;
			private Random bool = new Random();
			private List<Boolean> list = new LinkedList<>();

			@Override
			public Optional<Boolean> next() {
				if(this.counter < size) {
					Optional<Boolean> ris = Optional.of(this.bool.nextBoolean());
					this.list.add(ris.get());
					this.counter++;
					return ris;
				}
				else {
					return Optional.empty();
				}
			}

			@Override
			public List<Boolean> allSoFar() {
				return this.list;
			}

			@Override
			public PowerIterator<Boolean> reversed() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

}
