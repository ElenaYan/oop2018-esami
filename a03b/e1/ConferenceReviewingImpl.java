package a03b.e1;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ConferenceReviewingImpl implements ConferenceReviewing {
	
	private Map<Map<Question, Integer>, Integer> map;

	public ConferenceReviewingImpl() {
		this.map = new HashMap<>();
	}

	@Override
	public void loadReview(int article, Map<Question, Integer> scores) {
		this.map.put(scores, article);
	}

	@Override
	public void loadReview(int article, int relevance, int significance, int confidence, int fin) {
		final Map<Question, Integer> scores = new HashMap<>();
		scores.put(Question.RELEVANCE, relevance);
		scores.put(Question.SIGNIFICANCE, significance);
		scores.put(Question.CONFIDENCE, confidence);
		scores.put(Question.FINAL, fin);
		this.map.put(scores, article);
	}

	@Override
	public List<Integer> orderedScores(int article, Question question) {
		List<Integer> list = new LinkedList<>();		
		this.map.entrySet().stream().filter(e -> e.getValue().equals(article))
							.map(e -> e.getKey().get(question)).sorted().forEach(e -> list.add(e));
		return list; 
	}

	@Override
	public double averageFinalScore(int article) {
		return this.map.entrySet().stream()
				   .filter(e -> e.getValue() == article)
				   .mapToDouble(e -> e.getKey().get(Question.FINAL).doubleValue())
				   .average()
				   .getAsDouble();
	}

	@Override
	public Set<Integer> acceptedArticles() {
		Set<Integer> set = new HashSet<>();
 		for(int article : this.map.values()) {
			if(averageFinalScore(article) > 5 && (this.map.entrySet().stream().filter(e -> e.getValue() == article)
													.filter(e -> e.getKey().get(Question.RELEVANCE)>=8).count() >= 1)) {
				set.add(article);
			}
		}
		return set;
	}

	@Override
	public List<Pair<Integer, Double>> sortedAcceptedArticles() {
		List<Pair<Integer, Double>> list = new LinkedList<>();
		for(Integer i : acceptedArticles()) {
			list.add(new Pair<>(i, averageFinalScore(i)));
		}
		list.sort(new Comparator<>() {

			@Override
			public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
				return o2.getX().intValue()-o1.getX().intValue();
			}
			
		});
		return list;
	}

	private double averageConfidenceScore(int article) {
		return this.map.entrySet().stream()
				   .filter(e -> e.getValue() == article)
				   .mapToDouble(e -> e.getKey().get(Question.CONFIDENCE).doubleValue())
				   .average()
				   .getAsDouble();
	}
	
	@Override
	public Map<Integer, Double> averageWeightedFinalScoreMap() {
		Map<Integer, Double> m = new HashMap<>();
//		for(int article : this.map.values()) {
//			this.map.entrySet().stream()
//							   .filter(e -> e.getValue().equals(article))
//							   .forEach(e -> {						 
//								   Double aWeighted = (averageConfidenceScore(article) * averageFinalScore(article))/10; 
//								   m.put(article, aWeighted);			
//							   });
//		}
//		System.out.println(this.map + "\n**********\n" + m);
		return m;
	}

}
