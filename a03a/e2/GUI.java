package a03a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
	private static final long serialVersionUID = -6218820567019985015L;
	private static final int SIZE = 6;
	private Map<JButton, Pair<Integer,Integer>> buttons = new HashMap<>();
	
    private final Logics logics = new LogicsImpl();
    
    public GUI() {
    	
        this.setSize(400,400);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            
            Pair<Integer, Integer> hitPos = buttons.get(bt);
            logics.hit(hitPos.getX(), hitPos.getY());
            
            buttons.entrySet().stream().forEach(b -> {
            	Pair<Integer, Integer> pos = b.getValue();
            	if(b.getKey().isEnabled() && logics.isSetNewValue(pos.getX(), pos.getY())) {
            		Integer newValue;
            		if(b.getKey().getText().equals(" ")) {
            			newValue = 1;
            		}
            		else {
            			newValue = Integer.parseInt(b.getKey().getText())+1;
            		}
            		b.getKey().setText(newValue.toString());
            		b.getKey().setEnabled(logics.isEnabled(pos.getX(), pos.getY()));
            	}
            });
            
            logics.reset();
        };  
                               
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                this.buttons.put(jb, new Pair<>(i,j));
                panel.add(jb);
            }
        }
        this.setVisible(true);
        
        logics.init(SIZE);
        
    
        
    }
}
