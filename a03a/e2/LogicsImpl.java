package a03a.e2;

import java.util.HashMap;
import java.util.Map;

public class LogicsImpl implements Logics {
	
	private Map<Pair<Integer,Integer>, Integer> values = new HashMap<>();
	private Map<Pair<Integer,Integer>, Boolean> isAddValues = new HashMap<>();
	private Integer size;


	@Override
	public void init(int size) {
		this.size=size;
		
		for(int i=0; i<size; i++) {
			for(int j=0; j<size; j++) {
				this.values.put(new Pair<>(i,j), 0);
				this.isAddValues.put(new Pair<>(i,j), false);
			}
		}
	}

	@Override
	public void hit(int posX, int posY) {
		
		for(int i=posX-1; i<=posX+1; i++) {
			for(int j=posY-1; j<=posY+1; j++) {
				if(i>=0 && i<size && j>=0 && j<size) {
					Integer newValue = this.values.get(new Pair<>(i, j)) + 1;
					this.values.replace(new Pair<>(i, j), newValue);
				
					this.isAddValues.replace(new Pair<>(i,j), true);
				}
			}
		}
	}

	@Override
	public Boolean isEnabled(int posX, int posY) {
		return this.values.get(new Pair<>(posX, posY))<5;
	}

	@Override
	public Boolean isSetNewValue(int posX, int posY) {
		
		return this.isAddValues.get(new Pair<>(posX, posY));
	}

	@Override
	public void reset() {
		for(int i=0; i<this.size; i++) {
			for(int j=0; j<this.size; j++) {
				this.isAddValues.replace(new Pair<>(i,j), false);
			}
		}
	}
}
