package a03a.e2;

public interface Logics {

		void init(int size);
		
		void reset();
		
		public void hit(int posX, int posY);
		
		Boolean isSetNewValue(int posX, int posY);
		
		Boolean isEnabled(int posX, int posY);
}
