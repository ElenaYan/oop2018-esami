package a04.e1;

import java.util.List;
import java.util.Optional;
import java.util.Random;

public class IntegerIteratorsFactoryImpl implements IntegerIteratorsFactory {

	@Override
	public SimpleIterator<Integer> empty() {
		return new SimpleIterator<Integer>() {

			@Override
			public Optional<Integer> next() {
				return Optional.empty();
			}

		};
	}

	@Override
	public SimpleIterator<Integer> fromList(List<Integer> list) {
		return new SimpleIterator<Integer>() {
			
			private int counter = 0;
			
			@Override
			public Optional<Integer> next() {
				if(this.counter < list.size()) {
					return Optional.of(list.get(counter++));
				}
				return Optional.empty();
			}
		};
	}

	@Override
	public SimpleIterator<Integer> random(int size) {
		return new SimpleIterator<Integer>() {
			
			private int counter = 0;
			private Random number = new Random();

			@Override
			public Optional<Integer> next() {
				if(this.counter++ < size) {
					return Optional.of(number.nextInt(size));
				}
				return Optional.empty();
			}
			
			
		};
	}

	@Override
	public SimpleIterator<Integer> quadratic() {
		//1,2,2,3,3,3,4,4,4,4,5,5,...
		return new SimpleIterator<Integer>() {

			private int counter = 1;
			private int i=1;

			@Override
			public Optional<Integer> next() {				
				if(i <= this.counter) {	
					this.i++;			
				}
				else {
					this.counter++;
					this.i = 2;
				}
				System.out.println(this.counter);
				return Optional.of(this.counter);	
			}
			
		};
	}

	@Override
	public SimpleIterator<Integer> recurring() {
		// TODO Auto-generated method stub
		return null;
	}

}
